FROM java:8-jre-alpine

COPY target/hello-world-1.0-SNAPSHOT.jar /hello-world.jar
COPY hello-world.yml /

EXPOSE 8080

CMD ["java","-jar","/hello-world.jar","server","hello-world.yml"]

