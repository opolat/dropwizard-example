# Getting Started

## Build service
mvn clean package

## Create docker image
docker build -t opolat/dropwizard-example .
docker push opolat/dropwizard-example

## Deploy to minishift
```
oc new-app --docker-image="docker.io/opolat/dropwizard-example"
oc expose service dropwizard-example
```

## Test
http://dropwizard-example-myproject.192.168.99.106.nip.io/hello-world?name=xx